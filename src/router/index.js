import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home/Home'
import AboutUs from '../views/About/About'
import Dashboard from '../views/Dashboard/Dashboard'
import PageNotFound from '../views/Auth/NotFound'

Vue.use(VueRouter)

const routes = [
    {
        path: '/',
        redirect: '/dashboard'
    },
    {
        path: '/dashboard',
        name: 'dashboard',
        component: Dashboard
    },
    {
        path: '/home',  
        name: 'home',
        component: Home
    },
    {
        path: '/about-us',
        name: 'about-us',
        component: AboutUs
    },
    {
        path: '/404',
        name: 'page-not-found',
        component: PageNotFound
    },
    {
        path: '*',
        redirect: '/404'
    }
]

const router = new VueRouter({
    mode: 'history',
    base: process.env.BASE_URL,
    routes
})

export default router
