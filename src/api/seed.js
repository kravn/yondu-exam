export default {
    labels: ['JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN', 'JUL', 'AUG', 'SEP', 'OCT', 'NOV', 'DEC'],
    datasets: [
        {
            label: 'Sales',
            backgroundColor: '#40C4FF',
            data: [10, 4, 12, 16, 10, 18, 15, 5, 5, 13, 12, 15], 
        },
        {
            label: 'Revenue',
            backgroundColor: '#1DE9B6',
            data: [7, 7, 12, 17, 13, 13, 7, 1, 9, 6, 5, 10], 
        }
    ],
    highlights: [
        {
            title: 'Sales',
            tooltip: 'Sales: Lorem ipsum dolor.',
            body: [
                { label: 'Lifetime Sales', value: '175000', extra: 'P' },
                { label: 'Revenue Sales', value: '170000', extra: 'P' }
            ],
            footer: { label: 'Average Order Value', value: '1000000', extra: 'P' }
        },
        {
            title: 'Engagement',
            tooltip: 'Engagement: Lorem ipsum dolor.',
            body: [
                { label: 'Lifetime SMS Engagements', value: '455', extra: '' },
                { label: 'Daily Average', value: '15', extra: '' }
            ],
            footer: { label: '20% Decrease in 30 Days', value: '', extra: '' }
        },
        {
            title: 'Acquisition',
            tooltip: 'Acquisition: Lorem ipsum dolor.',
            body: [
                { label: 'Registered Users', value: '677', extra: '' },
                { label: 'Daily Average', value: '23', extra: '' }
            ],
            footer: { label: '20% Increase in 30 Days', value: '', extra: '' }
        },
        {
            title: 'Rewards Program',
            tooltip: 'Rewards Program: Lorem ipsum dolor.',
            body: [
                { label: 'Rewards Redeemed', value: '175000', extra: '' },
                { label: 'Issued Rewards', value: '175000', extra: '' }
            ],
            footer: { label: '20% Increase in 30 Days', value: '' }
        }
    ],
    performance_headers: {
        branch: [
            { id: '', value: 'id', sortable: false },
            { text: 'Branch', value: 'branch', sortable: false },
            { text: 'Total Sales', value: 'total_sales', sortable: false },
            { text: '*', value: 'activity', sortable: false }
        ],
        rewards: [
            { id: '', value: 'id', sortable: false },
            { text: 'Badge', value: 'branch', sortable: false },
            { text: 'Total Sales', value: 'total_sales', sortable: false },
            { text: '*', value: 'activity', sortable: false }
        ]
    },
    performance_data: {
        branch: [
            { id: 1, branch: 'Makati', total_sales: 12000, activity: '20', },
            { id: 2, branch: 'BGC Taguig', total_sales: 11000, activity: '18', },
            { id: 3, branch: 'Mall of Asia', total_sales: 10000, activity: '16', },
            { id: 4, branch: 'Quezon City', total_sales: 9000, activity: '14', },
            { id: 5, branch: 'Greenhills', total_sales: 8000, activity: '12', },
            { id: 6, branch: 'Ortigas Center', total_sales: 7000, activity: '10', },
            { id: 7, branch: 'Cubao', total_sales: 6000, activity: '9', },
            { id: 8, branch: 'Pasig', total_sales: 5000, activity: '8', },
            { id: 9, branch: 'Manila', total_sales: 4000, activity: '7', },
            { id: 10, branch: 'Alabang', total_sales: 3000, activity: '6', },
            { id: 11, branch: 'Bacoor', total_sales: 2000, activity: '5', },
            { id: 12, branch: 'Pateros', total_sales: 1000, activity: '4', },
        ],
        rewards: [
            { id: 1, branch: 'Reward 1', total_sales: 12000, activity: '20', },
            { id: 2, branch: 'Reward 2', total_sales: 11000, activity: '18', },
            { id: 3, branch: 'Reward 3', total_sales: 10000, activity: '16', },
            { id: 4, branch: 'Reward 4', total_sales: 9000, activity: '14', },
            { id: 5, branch: 'Reward 5', total_sales: 8000, activity: '12', },
            { id: 6, branch: 'Reward 6', total_sales: 7000, activity: '10', },
            { id: 7, branch: 'Reward 7', total_sales: 6000, activity: '9', },
            { id: 8, branch: 'Reward 8', total_sales: 5000, activity: '8', },
            { id: 9, branch: 'Reward 9', total_sales: 4000, activity: '7', },
            { id: 10, branch: 'Reward 10', total_sales: 3000, activity: '6', },
            { id: 11, branch: 'Reward 11', total_sales: 2000, activity: '5', },
            { id: 12, branch: 'Reward 12', total_sales: 1000, activity: '4', },
        ]
    },
}