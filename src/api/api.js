import endpoints from './endpoint'

function getSuccessResponse(res) {
    return {
        success: true,
        data: res.data
    }
}

function getErrorResponse(res) {
    return {
        success: false,
        data: []
    }
}

async function http(options) {
    let res
    try {
        res = await axios(options)
        return getSuccessResponse(res)
    } catch (error) {
        console.log(error.response)
        return getErrorResponse(res)
    }
}

export default {

    async validateToken() {
        return await http({
            url: endpoints.auth.validate_token
        })
    },

    async getToken(body) {
        return await http({
            url: endpoints.auth.get_token
        })
    },

    async destroyToken() {
        return await http({
            url: endpoints.auth.destroy_token
        })
    },

    async destroyToken() {
        return await axios.delete( endpoint.auth.destroy_token, getToken() )
    },

    async getSales() {
        return await http({
            url: endpoints.dashboard.get_sales
        })
    },

    async getEngagement() {
        return await http({
            url: endpoints.dashboard.get_engagement
        })
    },

    async getAcquisition() {
        return await http({
            url: endpoints.dashboard.get_acquisition
        })
    },

    async getRewardsProgram() {
        return await http({
            url: endpoints.dashboard.get_rewards_program
        })
    },

    async getSalesOverview() {
        return await http({
            url: endpoints.dashboard.get_sales_overview
        })
    },

    async getBranchPerformance(params) {
        return await http({
            url: endpoints.dashboard.get_branch_performance,
            params,
        })
    },

    async getRewardsPerformance(id) {
        return await http({
            url: `${endpoints.dashboard.get_rewards_performance}/${id}`,
        })
    },

}