export default {
	auth: {
        get_token: 'auth/token',
        destroy_token: 'auth/token/destroy',
        validate_token: 'auth/token/validate',
        get_logged_in_user: 'user/profile'
    },
	dashboard: {
		get_sales: `/analytics/sales`,
		get_engagement: `/analytics/engagement`,
		get_acquisition: `/analytics/acquisition`,
		get_rewards_program: `analytics/rewards-program`,
		get_sales_overview: `/analytics/sales-overview`,
		get_branch_performance: `/analytics/performance/branch`,
		get_rewards_performance: `/analytics/performance/rewards`,
	}
}