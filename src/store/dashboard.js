import Seeder from '../api/seed'
import { getField, updateField } from 'vuex-map-fields'

const state = {
    highlights: [],
    performance: {
        branch: {},
        rewards: {}
    },
    graph: {
        labels: [],
        datasets: []
    }
}

const getters = {
    getField,
    highlights: (state) => state.highlights,
    branch_performance: (state) => state.performance.branch,
    rewards_performance: (state) => state.performance.rewards,
    graph_data: (state) => state.graph
}

const actions = {
    getHighlights({commit}) {
        commit('onOkGetHighlights', Seeder.highlights)
    },
    getPerformance({commit}) {
        commit('onOkGetPerformance', {
            headers: Seeder.performance_headers,
            data: Seeder.performance_data
        })
    },
    getSalesOverview({commit}) {
        commit('onOkGetSalesOverview', {
            labels: Seeder.labels,
            datasets: Seeder.datasets
        })
    }
}

const mutations = {
    updateField,
    onOkGetHighlights(state, data) {
        state.highlights = data
    },
    onOkGetPerformance(state, data) {
        state.performance.branch = {
            headers: data.headers.branch,
            data: data.data.branch
        }
        state.performance.rewards = {
            headers: data.headers.rewards,
            data: data.data.rewards
        }
    },
    onOkGetSalesOverview(state, data) {
        state.graph.labels = data.labels
        state.graph.datasets = data.datasets
    }
}

export const dashboard = {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}