import Api from '../api/api'
import { getField, updateField } from 'vuex-map-fields'
import Cookie from 'vue-cookies'

const state = {
    welcome_modal: false,
    error_messages: {}
}

const getters = {
    getField,
    getWelcomeModal: (state) => { return state.welcome_modal },
    getErrorMessages: (state) => { return state.error_messages }
}

const actions = {
    async getToken({commit}, params = {}) {
        let response = await Api.getToken(params)
        if (response.status == 200) {
            window.localStorage.setItem('userdata', JSON.stringify(response.data.data))
            Cookie.set('token', response.data.data.token)
            window.location.reload()
        }
        commit('onOkValidateLogin', response.data)
    },
    async destroyToken() {
        return await Api.destroyToken({token: Cookie.get('token')})
    },
    async validateToken() {
        return await Api.validateToken({token: Cookie.get('token')})
    }
}

const mutations = {
    updateField,
    onOkValidateLogin(state, data) {
        if (data.errors) {
            state.error_messages = data.errors
        }
    },
    clearErrors(state) {
        state.error_messages = {}
    }
}

export const auth = {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}