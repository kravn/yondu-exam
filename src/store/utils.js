import { getField, updateField } from 'vuex-map-fields'

const state = {
    drawer: false
}

const getters = {
    getField,
    drawer: (state) => { state.drawer }
}

const actions = {
    
}

const mutations = {
    updateField,
    toggleNavigationDrawer(state) {
        state.drawer = !state.drawer
    }
}

export const utils = {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}