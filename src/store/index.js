import Vue from 'vue'
import Vuex from 'vuex'
import { getField, updateField } from 'vuex-map-fields'

import { dashboard } from './dashboard.js'
import { utils } from './utils.js'

Vue.use(Vuex)

const store = new Vuex.Store({
    namespaced: true,
    modules: {
        dashboard,
        utils
    },
    state:{
	
	},
	getters: {
		getField,
	},
	actions: {
	
	},
	mutations: {
		updateField,
	}
})

export default store