import { Bar } from 'vue-chartjs'
import { mapGetters, mapActions } from 'vuex'
 
export default {
    extends: Bar,

    mounted () {
        this.getSalesOverview()
        this.renderChart({
            labels: this.graph_data.labels,
            datasets: this.graph_data.datasets
        })
    },

    computed: { ...mapGetters('dashboard', ['graph_data']) },

    methods: {
        ...mapActions('dashboard', ['getSalesOverview'])
    },

    data: () => ({
        
    })
}