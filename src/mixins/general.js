import { mapFields } from 'vuex-map-fields'
import { mapMutations } from 'vuex'

import DatePicker from '../components/Base/DatePicker'

export default {
    name: 'GeneralMixins',
    computed: {
        ...mapFields('utils', ['drawer']),
        navigation_drawer: {
            get: function () {
                return this.drawer
            },
            set: function (newValue) {
                this.drawer = newValue
            }
        }
    },
    components: {
        DatePicker
    },
    methods: {
        ...mapMutations('utils', [
            'toggleNavigationDrawer'
        ]),
        toggleDrawer() {
            this.toggleNavigationDrawer()
        },
    },

    data: () => ({
        
    })
}